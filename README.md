# dre_whoops

Bessere Fehler Seiten

idrsa.pub mit dem Repository verheiraten

Repo verfügbar machen 
```php
    composer config repositories.bodynova/dre_whoops git git@bitbucket.org:bodynova/dre_whoops.git
```

```php
composer require bender/dre_whoops
```


# ! Ganz wichtig folgende Zeilen in der Datei: 

functions.php hinzufügen:

```php
/* Lines for dre_whoops */
#efine('__ROOT__', dirname(__FILE__, 2));
require_once(__ROOT__ . '/modules/bender/dre_whoops/functions.php');
```
