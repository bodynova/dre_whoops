<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

/**
 * Module information
 */
$aModule = [
    'id'          => 'dre_whoops',
    'title'       => '<img src="../modules/bender/dre_whoops/out/img/favicon.ico" title="Bender Fehler Behandlungs Modul">odynova Whoops Exception Handler',
    'description' => 'Verbesserte Fehler behandlung',
    'thumbnail'   => 'out/img/logo_bodynova.png',
    'version'     => '2.0.0',
    'author'      => 'Andre Bender',
    'url'         => 'https://bodynova.de',
    'email'       => 'a.bender@bodynova.de',
    'extend'      => [
    ],
    'settings'    => [
    ],
    'blocks'      => [
    ],
];
